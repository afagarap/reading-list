# reading-list

Repository dump for papers that I find interesting but haven't found the time to read them yet.

| Paper                                                                                                                                                                        | Method   | Venue        | Code                                           |
| ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------- | ------------ | ---------------------------------------------- |
| [Using Self-Supervised Learning Can Improve Model Robustness and Uncertainty](https://arxiv.org/abs/1906.12340)                                                              | SS-OoD   | NeurIPS 2019 | [PyTorch](https://github.com/hendrycks/ss-ood) |
| [Deep Kronecker neural networks: A general framework for neural networks with adaptive activation functions adaptive activation functions](https://arxiv.org/abs/2105.09513) | KNN      |              |                                                |
| [Multiplying Matrices without Multiplying](https://arxiv.org/pdf/2106.10860.pdf)                                                                                             | MADDNESS | ICML 2021    | https://github.com/dblalock/bolt               |
| [A Manifold Learning Perspective on Representation Learning: Learning Decoder and Representations without and Encoder](https://arxiv.org/pdf/2108.13910.pdf)                 |          |              |                                                |
| [Divide and Contrast: Self-supervised Learning from Uncurated Data](https://arxiv.org/pdf/2105.08054.pdf)                                                                    | DnC      |              |                                                |
| [S4L: Self-Supervised Semi-Supervised Learning](https://arxiv.org/abs/1905.03670)                                                                    | S4L      |              ICCV 2019 |                                                |
| [Explaining in Style: Training a GAN to explain a classifier in StyleSpace](https://arxiv.org/abs/2104.13369) | StylEx | ICCV 2021 | https://github.com/google/explaining-in-style | 
